import { Component, ElementRef, OnInit } from '@angular/core';
import { fromEvent, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'resizable-div',
  templateUrl: './resizable-div.component.html',
  styleUrls: ['./resizable-div.component.scss']
})
export class ResizableDivComponent implements OnInit {
  height: number = 300;
  minHeight: number = 150;
  resizing: boolean = false;
  mouseMoveListener: Observable<MouseEvent> = fromEvent(window, 'mousemove') as Observable<MouseEvent>;
  mouseUpListener: Observable<MouseEvent> = fromEvent(window, 'mouseup') as Observable<MouseEvent>;
  destroyed$: Subject<boolean> = new Subject<boolean>();

  startResize(event: MouseEvent) {
    if (event.button === 0) {
      this.resizing = true;
    }
  }

  stopResize() {
    this.resizing = false;
  }

  resize(clientY: number) {
    if (this.resizing) {
      let newHeight = clientY - this.el.nativeElement.getBoundingClientRect().top
      if (newHeight < this.minHeight) {
        newHeight = this.minHeight
      }
      this.height = newHeight;
    }
  }

  constructor(
    private el: ElementRef<HTMLDivElement>
  ) { }

  ngOnDestroy() {
    this.destroyed$.next(true);
  }

  ngOnInit(): void {
    this.mouseMoveListener
      .pipe(takeUntil(this.destroyed$))
      .subscribe(event => this.resize(event.clientY))
    this.mouseUpListener
      .pipe(takeUntil(this.destroyed$))
      .subscribe(_ => this.stopResize())
  }

}
